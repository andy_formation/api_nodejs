var express = require("express");
var app = express();
const cors = require('cors')

var sqlite3 = require('sqlite3').verbose();
const db_name = "etudiantsDB";
let db = new sqlite3.Database(db_name, (err) => {
    if (err) {
      return console.error(err.message);
    }
    console.log('Connected to the SQlite database.');
  });

const bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// CORS

app.use(cors({
  origin: function(origin, callback){
    if(!origin) return callback(null, true);
    return callback(null, true);
  }
}));


const etudiantsPerPage = 5;

// Je fais ma requête sql dans la const sql et je l'execute via db all
// Mais pour chaque rows (=etudiant) j'appelle la fonction calculAge en lui passant en paramètre la date de naissance
// de l'etudiant et je rajoute a chaque rows une colonne "Age" qui prend la valeur de ce que retourne la fonction
// je renvoie ensuite la vue liste avec en paramètre le tableau des etudiants
app.get("/liste/:currentPage", (req, res) => {
    let currentPage = req.params.currentPage || 1;
    let first = (currentPage * etudiantsPerPage) - etudiantsPerPage;
    let nbEtudiantsSql = 'SELECT COUNT(*) as nb_etudiants FROM etudiants';
    db.get(nbEtudiantsSql, (err, result) => {
      if (err) {
      res.json(err.message);
      }
      let nbEtudiants = result.nb_etudiants;
      let page = Math.ceil(nbEtudiants / etudiantsPerPage);
      //if (currentPage > page) {currentPage = page} 
    const sql = "SELECT * FROM etudiants LIMIT ?, ?";
    db.all(sql, [first, etudiantsPerPage], (err, rows) => {
      if (err) {
        return res.json(err.message);
      }
      rows = rows.map(row => {
        row.age = calculAge(row.dateNaissance);
        return row;
      });
      res.json({ etudiants: rows, currentPage, page});   
    });
  });
});




//fonction qui calcul l'age et qui le retourne
//en paramètre on envoie la date de Naissance de l'etudiant qui est enregistrée dans la BDD
function calculAge(date) {
  var date = Array.from(date);
   date = date[5] + date[6] + "/" + date [8] + date [9]+ "/"+ date[0]+ date[1]+ date[2]
        + date[3]
  date = new Date(date);
  var month_diff = Date.now() -date.getTime();
  var age_dt = new Date(month_diff);
  var year = age_dt.getUTCFullYear();
  var age =  Math.abs(year - 1970);
  return(age);
}


app.post("/ajoutEtudiant", (req, res) => {
  const sql = "INSERT INTO etudiants (nom, prenom, dateNaissance, numCI) VALUES (?,?,?,?)";
  const params = [req.body.name, req.body.prenom, req.body.date, req.body.num];
  db.run(sql, params, (err) => {
    if (err) {
      return res.status(500).json(err.message);
    }
    res.status(200);
    res.json({message: 'étudiant ajouté !' }); 
  });
});

app.get("/etudiantByID/:id", (req, res) => {
  const sql = "SELECT * FROM etudiants WHERE idEtudiants= ?";
  const params = [req.params.id];
    db.get(sql, params, (err, etudiant) => {
      if (err) {
        return res.status(500).json(err.message);
      }
      res.json({ etudiant: etudiant});  
    });
});



app.post("/modifEtudiant/:id/", (req, res) => {
  const sql = "UPDATE etudiants SET idEtudiants = ?, nom = ?, prenom = ?, dateNaissance = ?, numCI = ? WHERE idEtudiants = ?";
  const params = [req.params.id, req.body.name, req.body.prenom, req.body.date, req.body.num, req.params.id];
  console.log(params)
  db.run(sql, params, (err) => {
    if (err) {
      return res.json(err.message);
    }
    res.json({message: 'etudiant modifié !'});   
  });
});

app.get("/suppEtudiant/:id", (req, res) => {
  const sql = "DELETE from etudiants WHERE idEtudiants = ?";
  //pas req.body car je vais récupérer les données dans l'URL
  const params = [req.params.id];
  db.run(sql, params, (err) => {
    if (err) {
      return res.json(err.message);
    }
  }); 
  res.json({message: 'étudiant supprimé !'});  
});


app.post("/listeBySearch/:research", (req, res) => {
  const sql = "SELECT * FROM etudiants WHERE nom LIKE ? OR prenom LIKE ?";
  const params = [req.params.research+'%', req.params.research+'%'];
  db.all(sql, params, (err, rows) => {
    if (err) {
      return res.json(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
    res.json({ etudiants: rows, currentPage: "", page: ""});   
  });
});



/*app.post("/liste/:currentPage/:search", (req, res) => {
  let currentPage = req.params.currentPage || 1;
  let first = (currentPage * etudiantsPerPage) - etudiantsPerPage;
  let nbEtudiantsSql = 'SELECT COUNT(*) as nb_etudiants FROM etudiants';
  db.get(nbEtudiantsSql, (err, result) => {
    if (err) {
    console.error(err.message);
    }
    let nbEtudiants = result.nb_etudiants;
    let page = Math.ceil(nbEtudiants / etudiantsPerPage);
 
  const sql = "SELECT * FROM etudiants LIMIT ?, ?";
  if (req.query.search) {
    sql += "WHERE nom LIKE ? OR prenom LIKE ?"
  
  const params = [req.body.research+'%', req.body.research+'%'];
  db.all(sql, [params], [first, etudiantsPerPage], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
 
    res.render('liste', { etudiants: rows, currentPage, page});   
  
  });
 } else {
  const sql = "SELECT * FROM etudiants LIMIT ?, ?";
  db.all(sql, [first, etudiantsPerPage], (err, rows) => {
    if (err) {
      return console.error(err.message);
    }
    rows = rows.map(row => {
      row.age = calculAge(row.dateNaissance);
      return row;
    });
    res.render('liste', { etudiants: rows, currentPage, page});   
  });
 }
});
});*/


app.listen(1337);
console.log('Server running at http://127.0.0.1:1337');